#!/bin/bash
PROJ_PATH=$(curl --request POST \
	         --header "Content-Type: application/json" \
                 --header "PRIVATE-TOKEN: jd5YgPiD6qmtxzmpfMAu" \
		 --data "{ \"ref\": \"dev\", \"variables\": [ {\"key\": \"NGX_NAME\", \"value\": \"${1}\"},{\"key\": \"APP_NAME\",\"value\": \"${2}\"}, {\"key\": \"PORT\", \"value\": \"8001\"} ] }" "https://gitlab.com/api/v4/projects/23693118/pipeline" | jq .[]|awk -F"details_path\":" '{print $2}' | sed '/^$/d')
echo "PROJ_PATH: $PROJ_PATH"
PIP_ID=$(echo ${PROJ_PATH##*/}| cut -d"\"" -f1)
echo "PIP_ID: $PIP_ID"
ID=$(curl -k --location --header "PRIVATE-TOKEN: jd5YgPiD6qmtxzmpfMAu" "https://gitlab.com/api/v4/projects/23693118/pipelines/$PIP_ID/jobs" | jq '.[] | select(.name == "build_dev") | .id')                            
echo "ID: $ID"
#run                                                                                                            
curl -k --request POST --header "PRIVATE-TOKEN: jd5YgPiD6qmtxzmpfMAu" "https://gitlab.com/api/v4/projects/23693118/jobs/$ID/play"

